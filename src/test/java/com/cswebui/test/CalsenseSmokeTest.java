package com.cswebui.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import static org.junit.Assert.*;
public class CalsenseSmokeTest {
	
	public String pageTitleCS = "Calsense Command Center";
	private WebDriver driver;
	
	@Before
	public void InitTest()
	{
	    driver = new HtmlUnitDriver();
		driver.get("http://web-ui.azurewebsites.net/");
	}
	
	
	@Test
	public void VerifyCSTitle()
	{
		String pageTitle = driver.getTitle();
    	assertEquals("Title not match or not found", pageTitleCS, pageTitle);
    	System.out.println("Title page text = " + pageTitle);
	}
	
	@After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
